#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <thread>
#include <chrono>

#define LOG_NAME "TraceTXT.TXT"
#define CSV_1_NAME "PP1-"
#define CSV_2_NAME "PP2-"

/* Holds the required sensor data for a PP1 frame */

struct pp_1_frame
{
	std::string rpm;
	std::string cam_crank_sync;
	std::string fuel_pres;
	std::string fuel_pres_diff;
	std::string fpr_duty;
	std::string fuel_flow;
	std::string inj_corr_1;
	std::string inj_corr_2;
	std::string inj_corr_3;
	std::string inj_corr_4;
	std::string inj_14_volt;
	std::string inj_23_volt;
	std::string mass_airflow;
	std::string egr_valve_duty;
	std::string egr_throttle_duty;
};

/* Holds the required sensor data for a PP2 frame */

struct pp_2_frame
{
	std::string rpm;
	std::string fpr_current;
	std::string inj_pre_adv;
	std::string inj_main_adv;
	std::string third_piston_deac;
	std::string turbo_pres_meas;
	std::string turbo_pres_ref;
	std::string wastegate_duty;
	std::string atmospheric_pres;
	std::string coolant_temp;
	std::string air_temp;
	std::string fuel_temp;
	std::string inj_flow;
};

/* Trim each PP1 frame string, and leave only the sensor values */

void trimPP1(std::vector<pp_1_frame>& frames)
{
	try
	{
		if (frames.empty()) throw std::exception();

		for (pp_1_frame& f : frames)
		{
			f.rpm = f.rpm.substr(0, f.rpm.length() - 5);
			f.rpm = f.rpm.substr(14, f.rpm.length());

			f.cam_crank_sync = f.cam_crank_sync.substr(0, f.cam_crank_sync.length() - 1);
			f.cam_crank_sync = f.cam_crank_sync.substr(37, f.cam_crank_sync.length());

			f.fuel_pres = f.fuel_pres.substr(0, f.fuel_pres.length() - 9);
			f.fuel_pres = f.fuel_pres.substr(24, f.fuel_pres.length());

			f.fuel_pres_diff = f.fuel_pres_diff.substr(0, f.fuel_pres_diff.length() - 9);
			f.fuel_pres_diff = f.fuel_pres_diff.substr(76, f.fuel_pres_diff.length());

			f.fpr_duty = f.fpr_duty.substr(0, f.fpr_duty.length() - 2);
			f.fpr_duty = f.fpr_duty.substr(31, f.fpr_duty.length());

			f.fuel_flow = f.fuel_flow.substr(0, f.fuel_flow.length() - 12);
			f.fuel_flow = f.fuel_flow.substr(25, f.fuel_flow.length());

			f.inj_corr_1 = f.inj_corr_1.substr(0, f.inj_corr_1.length() - 12);
			f.inj_corr_1 = f.inj_corr_1.substr(28, f.inj_corr_1.length());

			f.inj_corr_2 = f.inj_corr_2.substr(0, f.inj_corr_2.length() - 12);
			f.inj_corr_2 = f.inj_corr_2.substr(28, f.inj_corr_2.length());

			f.inj_corr_3 = f.inj_corr_3.substr(0, f.inj_corr_3.length() - 12);
			f.inj_corr_3 = f.inj_corr_3.substr(28, f.inj_corr_3.length());

			f.inj_corr_4 = f.inj_corr_4.substr(0, f.inj_corr_4.length() - 12);
			f.inj_corr_4 = f.inj_corr_4.substr(28, f.inj_corr_4.length());

			f.inj_14_volt = f.inj_14_volt.substr(0, f.inj_14_volt.length() - 9);
			f.inj_14_volt = f.inj_14_volt.substr(27, f.inj_14_volt.length());

			f.inj_23_volt = f.inj_23_volt.substr(0, f.inj_23_volt.length() - 9);
			f.inj_23_volt = f.inj_23_volt.substr(27, f.inj_23_volt.length());

			f.mass_airflow = f.mass_airflow.substr(0, f.mass_airflow.length() - 12);
			f.mass_airflow = f.mass_airflow.substr(19, f.mass_airflow.length());

			f.egr_valve_duty = f.egr_valve_duty.substr(0, f.egr_valve_duty.length() - 3);
			f.egr_valve_duty = f.egr_valve_duty.substr(28, f.egr_valve_duty.length());

			f.egr_throttle_duty = f.egr_throttle_duty.substr(0, f.egr_throttle_duty.length() - 3);
			f.egr_throttle_duty = f.egr_throttle_duty.substr(31, f.egr_throttle_duty.length());
		}
		std::cout << "[INFO]: Parsing successful" << std::endl;
	}
	catch (std::exception&)
	{
		std::cout << "[ERROR]: Parsing unsuccessful" << std::endl;
	}
}

/* Trim each PP2 frame string, and leave only the sensor values */

void trimPP2(std::vector<pp_2_frame>& frames)
{
	try
	{
		if (frames.empty()) throw std::exception();

		for (pp_2_frame& f : frames)
		{
			f.rpm = f.rpm.substr(0, f.rpm.length() - 5);
			f.rpm = f.rpm.substr(14, f.rpm.length());

			f.fpr_current = f.fpr_current.substr(0, f.fpr_current.length() - 3);
			f.fpr_current = f.fpr_current.substr(35, f.fpr_current.length());

			f.inj_pre_adv = f.inj_pre_adv.substr(0, f.inj_pre_adv.length() - 2);
			f.inj_pre_adv = f.inj_pre_adv.substr(23, f.inj_pre_adv.length());

			f.inj_main_adv = f.inj_main_adv.substr(0, f.inj_main_adv.length() - 2);
			f.inj_main_adv = f.inj_main_adv.substr(24, f.inj_main_adv.length());

			f.third_piston_deac = f.third_piston_deac.substr(0, f.third_piston_deac.length() - 1);
			f.third_piston_deac = f.third_piston_deac.substr(25, f.third_piston_deac.length());

			f.turbo_pres_meas = f.turbo_pres_meas.substr(0, f.turbo_pres_meas.length() - 7);
			f.turbo_pres_meas = f.turbo_pres_meas.substr(25, f.turbo_pres_meas.length());

			f.turbo_pres_ref = f.turbo_pres_ref.substr(0, f.turbo_pres_ref.length() - 7);
			f.turbo_pres_ref = f.turbo_pres_ref.substr(32, f.turbo_pres_ref.length());

			f.wastegate_duty = f.wastegate_duty.substr(0, f.wastegate_duty.length() - 3);
			f.wastegate_duty = f.wastegate_duty.substr(33, f.wastegate_duty.length());

			f.atmospheric_pres = f.atmospheric_pres.substr(0, f.atmospheric_pres.length() - 7);
			f.atmospheric_pres = f.atmospheric_pres.substr(22, f.atmospheric_pres.length());

			f.coolant_temp = f.coolant_temp.substr(0, f.coolant_temp.length() - 8);
			f.coolant_temp = f.coolant_temp.substr(21, f.coolant_temp.length());

			f.air_temp = f.air_temp.substr(0, f.air_temp.length() - 8);
			f.air_temp = f.air_temp.substr(17, f.air_temp.length());

			f.fuel_temp = f.fuel_temp.substr(0, f.fuel_temp.length() - 8);
			f.fuel_temp = f.fuel_temp.substr(19, f.fuel_temp.length());

			f.inj_flow = f.inj_flow.substr(0, f.inj_flow.length() - 12);
			f.inj_flow = f.inj_flow.substr(25, f.inj_flow.length());
		}
		std::cout << "[INFO]: Parsing successful" << std::endl;
	}
	catch (std::exception&)
	{
		std::cout << "[ERROR]: Parsing unsuccessful" << std::endl;
	}
}

/* Output the parsed PP1 frames to a csv file */

void csvPP1(const std::vector<pp_1_frame>& frames, int fileCount)
{
	std::ofstream csv_file;

	csv_file.open(CSV_1_NAME + std::to_string(fileCount) + ".csv");

	csv_file <<
		"RPM,Cam/Crank Synchronised,"
		"Fuel Pressure,"
		"Fuel Pressure Delta,"
		"Fuel Pressure Regulator Duty Cycle,"
		"Fuel Flow,Injector Correction 1,"
		"Injector Correction 2,"
		"Injector Correction 3,"
		"Injector Correction 4,"
		"Injector 1/4 Voltage,"
		"Injector 2/3 Voltage,"
		"Mass Airflow,"
		"Exhaust Gas Recirculator Duty Cycle,"
		"Throttle Duty Cycle" << std::endl;

	for (const pp_1_frame& f : frames)
		csv_file <<
		f.rpm << "," <<
		f.cam_crank_sync << "," <<
		f.fuel_pres << "," <<
		f.fuel_pres_diff << "," <<
		f.fpr_duty << "," <<
		f.fuel_flow << "," <<
		f.inj_corr_1 << "," <<
		f.inj_corr_2 << "," <<
		f.inj_corr_3 << "," <<
		f.inj_corr_4 << "," <<
		f.inj_14_volt << "," <<
		f.inj_23_volt << "," <<
		f.mass_airflow << "," <<
		f.egr_valve_duty << "," <<
		f.egr_throttle_duty << std::endl;

	std::cout << "[INFO]: Data written to csv file\n" << std::endl;
	csv_file.close();
}

/* Output the parsed PP2 frames to a csv file */

void csvPP2(const std::vector<pp_2_frame>& frames, int fileCount)
{
	std::ofstream csv_file;

	csv_file.open(CSV_2_NAME + std::to_string(fileCount) + ".csv");
	csv_file <<
		"RPM,"
		"Fuel Pressure Regulator Current,"
		"Pre-injection Advance,"
		"Main-injection advance,"
		"Third Piston Deactivation,"
		"Turbo Pressure Measured,"
		"Turbo Pressure Reference,"
		"Wastegate Duty Cycle,"
		"Atmospheric Pressure,"
		"Coolant Temperature,"
		"Air Temperature,"
		"Fuel Temperature,"
		"Injected Quantity" << std::endl;

	for (const pp_2_frame& f : frames)
		csv_file <<
		f.rpm << "," <<
		f.fpr_current << "," <<
		f.inj_pre_adv << "," <<
		f.inj_main_adv << "," <<
		f.third_piston_deac << "," <<
		f.turbo_pres_meas << "," <<
		f.turbo_pres_ref << "," <<
		f.wastegate_duty << "," <<
		f.atmospheric_pres << "," <<
		f.coolant_temp << "," <<
		f.air_temp << "," <<
		f.fuel_temp << "," <<
		f.inj_flow << std::endl;

	std::cout << "[INFO]: Data written to csv file\n" << std::endl;
	csv_file.close();
}

/* Read in a PP1 log from the text file */

bool loadPP1(std::vector<pp_1_frame>& frames, std::ifstream& log_file)
{
	std::string current_line;
	bool firstRead = true;

	/* This method finds the start of a frame and reads it in */

	while (std::getline(log_file, current_line))
	{
		/* Because there is only one open stream on the TraceTXT file, the entry point
		for this method will change.
		* If this is the first frame of a new log, the entry point in the text file
		will be the blank line above "Engine Speed: ...". We are already in the right
		place to read a frame in.
		* If we've already read an entry from the log file, we will only need to go down
		one line to get to the start of the sensor data.
		* If its not our first read, and the current line isn't "Principal Parameters 1",
		we have reached the end of this log.
		*/

		if (!firstRead && current_line != "Principal parameters 1")
			break;

		if (!firstRead)
			std::getline(log_file, current_line);

		pp_1_frame f;

		std::getline(log_file, f.rpm);
		std::getline(log_file, f.cam_crank_sync);
		std::getline(log_file, f.fuel_pres);
		std::getline(log_file, f.fuel_pres_diff);
		std::getline(log_file, f.fpr_duty);
		std::getline(log_file, f.fuel_flow);
		std::getline(log_file, f.inj_corr_1);
		std::getline(log_file, f.inj_corr_2);
		std::getline(log_file, f.inj_corr_3);
		std::getline(log_file, f.inj_corr_4);
		std::getline(log_file, f.inj_14_volt);
		std::getline(log_file, f.inj_23_volt);
		std::getline(log_file, f.mass_airflow);
		std::getline(log_file, f.egr_valve_duty);
		std::getline(log_file, f.egr_throttle_duty);

		getline(log_file, current_line);

		/* At this point we expect to be at the end of the frame, and the current line
		should read "Frame :" If it doesn't, the log file is in a different format
		and cannot be parsed.
		*/

		if (current_line != "Frame :")
		{
			std::cout << "[INFO]: Principal parameters 1 log detected" << std::endl;
			std::cout << "[ERROR]: Log frame not in expected format" << std::endl;
			std::cout << "[INFO]: Restarting from next complete frame\n" << std::endl;
			return false;
		}

		/* Once we've validated the frame was in the expected format, we need to skip
		over the irrelevant frame metadata at the end. We keep reading in line after
		line until we reach the "=" line, which signifies the end of a frame.
		*/

		do
		{
			getline(log_file, current_line);
		} while (current_line != "===========================================================");

		frames.push_back(f);
		firstRead = false;
	}

	std::cout << "[INFO]: Principal parameters 1 log detected" << std::endl;
	std::cout << "[INFO]: Number of frames: " << frames.size() << std::endl;
	return true;
}

/* Read in a PP2 log from the text file */

bool loadPP2(std::vector<pp_2_frame>& frames, std::ifstream& log_file)
{
	std::string current_line;
	bool firstRead = true;

	/* This method finds the start of a frame and reads it in */

	while (std::getline(log_file, current_line))
	{
		/* Because there is only one open stream on the TraceTXT file, the entry point
		for this method will change.
		* If this is the first frame of a new log, the entry point in the text file
		will be the blank line above "Engine Speed: ...". We are already in the right
		place to read a frame in.
		* If we've already read an entry from the log file, we will only need to go down
		one line to get to the start of the sensor data.
		* If its not our first read, and the current line isn't "Principal Parameters 1",
		we have reached the end of this log.
		*/

		if (!firstRead && current_line != "Principal parameters 2")
			break;

		if (!firstRead)
			std::getline(log_file, current_line);

		pp_2_frame f;

		std::getline(log_file, f.rpm);
		std::getline(log_file, f.fpr_current);
		std::getline(log_file, f.inj_pre_adv);
		std::getline(log_file, f.inj_main_adv);
		std::getline(log_file, f.third_piston_deac);
		std::getline(log_file, f.turbo_pres_meas);
		std::getline(log_file, f.turbo_pres_ref);
		std::getline(log_file, f.wastegate_duty);
		std::getline(log_file, f.atmospheric_pres);
		std::getline(log_file, f.coolant_temp);
		std::getline(log_file, f.air_temp);
		std::getline(log_file, f.fuel_temp);
		std::getline(log_file, f.inj_flow);

		getline(log_file, current_line);

		/* At this point we expect to be at the end of the frame, and the current line
		should read "Frame :" If it doesn't, the log file is in a different format
		and cannot be parsed.
		*/

		if (current_line != "Frame :")
		{
			std::cout << "[INFO]: Principal parameters 2 log detected" << std::endl;
			std::cout << "[ERROR]: Log not in expected format\n" << std::endl;
			return false;
		}

		/* Once we've validated the frame was in the expected format, we need to skip
		over the irrelevant frame metadata at the end. We keep reading in line after
		line until we reach the "=" line, which signifies the end of a frame.
		*/

		do
		{
			getline(log_file, current_line);
		} while (current_line != "===========================================================");

		frames.push_back(f);
		firstRead = false;
	}

	std::cout << "[INFO]: Principal parameters 2 log detected" << std::endl;
	std::cout << "[INFO]: Number of frames: " << frames.size() << std::endl;
	return true;
}

/* Handle the PP1 load, conversion and output methods */

bool handlePP1(std::ifstream& log_file, int& fileCountPP1)
{
	std::vector<pp_1_frame> frames;

	/* If we manage to load the PP1 log, trim the frames and export to csv file */

	if (loadPP1(frames, log_file) == true)
	{
		trimPP1(frames);
		csvPP1(frames, fileCountPP1);
		return true;
	}
	return false;
}

/* Handle the PP2 load, conversion and output methods */

bool handlePP2(std::ifstream& log_file, int& fileCountPP2)
{
	std::vector<pp_2_frame> frames;

	/* If we manage to load the PP2 log, trim the frames and export to csv file */

	if (loadPP2(frames, log_file) == true)
	{
		trimPP2(frames);
		csvPP2(frames, fileCountPP2);
		return true;
	}
	return false;
}

/* Iterate through the file, search the file for all logs */

void processFile()
{
	std::ifstream log_file;
	std::string current_line;
	const auto start = std::chrono::high_resolution_clock::now();
	bool foundLog = false;
	int fileCountPP1 = 0, fileCountPP2 = 0;	//Used for naming the output csv files

	log_file.open(LOG_NAME);

	/* This method will keep scanning through the file until it finds the start of a PP1 or PP2 frame.
	Once the start of a log is found, we call either handlePP1 or handlePP2 to deal with the log.
	*/

	if (log_file)
	{
		std::cout << "[INFO]: Opened log file\n" << std::endl;

		while (std::getline(log_file, current_line))
		{
			if (current_line == "Principal parameters 1")
			{
				if (handlePP1(log_file, fileCountPP1) == true)
					foundLog = true;
				fileCountPP1++;
			}
			else if (current_line == "Principal parameters 2")
			{
				if (handlePP2(log_file, fileCountPP2) == true)
					foundLog = true;
				fileCountPP2++;
			}
		}

		if (foundLog)
		{
			std::cout << "[INFO]: Closed log file" << std::endl;
			std::cout << "[INFO]: Closed csv files" << std::endl;
			std::cout << "\nFile conversion successful" << std::endl;
		}
		else
		{
			std::cout << "[ERROR]: No sensor data found in log file\n" << std::endl;
			std::cout << "[INFO]: Closed log file" << std::endl;
			std::cout << "\nFile conversion failed" << std::endl;
		}
	}
	else
	{
		std::cout << "\n[ERROR]: Cannot find log file" << std::endl;
		std::cout << "\nFile conversion failed" << std::endl;
	}

	log_file.close();

	/* Timer */

	const auto end = std::chrono::high_resolution_clock::now();
	auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	std::cout << "Operation completed in " << milliseconds.count() << "ms" << std::endl;
}

int main()
{
	std::string current_line;

	std::cout << "Peugeot Planet Log Converter" << std::endl;
	std::cout << "----------------------------" << std::endl;
	std::cout << "Please ensure the 'TraceTXT.txt' log file is in the same directory as this executable" << std::endl;
	std::cout << "Press any key to continue...";
	std::cin.get();
	std::cout << std::endl;

	processFile();

	std::cout << "Press any key to exit..." << std::endl;
	std::cin.get();

	return 0;
}